#ifndef IMAGE_MANIPULATOR_IMAGE_WINDOW_H
#define IMAGE_MANIPULATOR_IMAGE_WINDOW_H

#include "JpegImage.hpp"

#include <QLabel>
#include <QSize>


class Kernel;

class ImageWindow : public QLabel
{
	Q_OBJECT
public:
	ImageWindow(QString& path, QWidget* parent=nullptr);
	void apply(const Kernel& kernel);
	void reload();
	void save(QString str);
	void closeEvent(QCloseEvent *event) override;

private:
	QSize windowSize_;
	QString imagePath_;
	JpegImage image_;

signals:
	void windowClosed();
	
};


#endif