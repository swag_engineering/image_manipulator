#ifndef IMAGE_MANIPULATOR_CUSTOM_FRAME_H
#define IMAGE_MANIPULATOR_CUSTOM_FRAME_H

#include "EmptyFrame.hpp"

#include <QLineEdit>
#include <QList>

class CustomFrame3x3 : public EmptyFrame
{
	Q_OBJECT
public:
	CustomFrame3x3(QWidget* parent = nullptr);
	Kernel getKernel() override;

private:
	unsigned int size_;
	QList<QList<QLineEdit*>> lineEditGrid_;

	double getValue(int y, int x);
	static double translator(void* context, int y, int x);
};


#endif