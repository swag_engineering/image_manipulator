#ifndef IMAGE_MANIPULATOR_IMAGE_MANIPULATOR_H
#define IMAGE_MANIPULATOR_IMAGE_MANIPULATOR_H

#include <QComboBox>
#include <QPushButton>
#include <QHBoxLayout>
#include <QString>
#include <QMap>

class JpegImage;
class EmptyFrame;
class ImageWindow;


template<typename T> EmptyFrame* createFrameInstance() { return new T; }

class ImageManipulator : public QWidget
{
	Q_OBJECT
public:
	explicit ImageManipulator(QWidget *parent = nullptr);

private:
	QHBoxLayout* frameLayout_;
	QPushButton* openButton_;
	QPushButton* reloadButton_;
	QPushButton* saveButton_;
	QPushButton* applyButton_;
	EmptyFrame* frame_;
	QComboBox* filterSelector_;

	ImageWindow* dialog_;
	QThread* thread_;

	QMap<QString, EmptyFrame*(*)()> filterSelectrionMap_;

	void errorBox(QString text);
	void warningBox(QString text);

public slots:
	void setPanelEnabled(bool state);
	void filterChanged(QString filterName);
	void openImage();
	void applyFilter();
	void saveImage();
		
};

#endif