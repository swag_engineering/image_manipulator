#ifndef IMAGE_MANIPULATOR_KERNEL_H
#define IMAGE_MANIPULATOR_KERNEL_H

#include <vector>

class Kernel
{
public:
	Kernel(unsigned int size, double (*valueSetter)(void*, int, int),
		void* context, bool normalize = false);
	size_t size() const { return size_; }
	std::vector<double> operator[](unsigned int x) const { return kernel_[x]; };

private:
	std::vector<std::vector<double>> kernel_;
	size_t size_;
	
};

#endif