#ifndef IMAGE_MANIPULATOR_JPEG_IMAGE_H
#define IMAGE_MANIPULATOR_JPEG_IMAGE_H

#include <QImage>

class Kernel;

class JpegImage : public QImage
{
public:
	JpegImage() : QImage(), channels_(0) {};
	JpegImage(const QString& fileName)
		: QImage(fileName), channels_(0) {};;
	JpegImage(const QString& fileName, const QSize& size);
	void convolute(const Kernel& kernel);

private:
	size_t channels_;
};

#endif
