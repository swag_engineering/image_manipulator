#ifndef IMAGE_MANIPULATOR_BASE_FRAME_H
#define IMAGE_MANIPULATOR_BASE_FRAME_H

#include "Kernel.hpp"
#include <QFrame>

class BaseFrame : public QFrame
{
	Q_OBJECT
public:
	BaseFrame(QWidget* parent = nullptr) : QFrame(parent)
		{ setFixedSize(QSize(width_, height_)); };
	size_t width() const { return width_; };
	size_t height() const { return height_; };
	virtual Kernel getKernel() = 0;
protected:
	size_t width_ = 300;
	size_t height_ = 420;

};

#endif