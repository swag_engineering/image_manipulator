#ifndef IMAGE_MANIPULATOR_EMPTY_FRAME_H
#define IMAGE_MANIPULATOR_EMPTY_FRAME_H

#include "BaseFrame.hpp"
#include "Kernel.hpp"

class Kernel;

class EmptyFrame : public BaseFrame
{
	Q_OBJECT
public:
	EmptyFrame(QWidget* parent = nullptr);
	Kernel getKernel() override { throw std::runtime_error("Choose filter first!"); }
	
};

#endif