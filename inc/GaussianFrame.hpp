#ifndef IMAGE_MANIPULATOR_GAUSSIAN_FRAME_H
#define IMAGE_MANIPULATOR_GAUSSIAN_FRAME_H

#include "EmptyFrame.hpp"
#include "qcustomplot.h"

#include <QSpinBox>
#include <QLabel>

class GaussianFrame : public EmptyFrame
{
	Q_OBJECT
public:
	GaussianFrame(QWidget* parent = nullptr);
	Kernel getKernel() override;
	static double translator(void* context, int y, int x);

private:
	QCustomPlot* customPlot_;
	QSpinBox* kernelSpinBox_;
	double sigmaValue_;
	double muValue_;
	size_t vectorSize_;
	QVector<double> y_ = QVector<double>(vectorSize_);
	QVector<double> x_ = QVector<double>(vectorSize_);

	void redrawPlot();
	double getValue(int y, int x);

public slots:
	void sigmaSliderChanged(int value);
	void muSliderChanged(int value);
	
};

#endif