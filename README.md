# Image manipulator
<div align="center"><img src="img/main_window.png" align="center" height="350"></div>

<div>That's just a simple and minimalistic program to play around with different kernel filters and evaluate the impact of each one on the final image.  
During my computer vision course I was not able to understand the magic behind filters and decided to write that program to make it intuitive and easy to compare result of different filters. The intention was to write my own implementation of convolution algorithm to understand it better and that is why it's not highly efficient, I have just slightly otpimized computation on edges.</div>


#### Requirements  

Project was tested with following versions:  
- Qt 5.12 / qmake 3.1
- g++ 9.2.1


#### Build 

``` shell
$ git clone https://gitlab.com/swag_engineering/image_manipulator.git
$ cd image_manipulator
$ qmake && make && ./build/bin/image_manipulator
```

#### Contributing  

If you want to create your own custom frame to generate kernel you can inherit from BaseFrame of EmptyFrame and add your frame in ImageManipulator's constructor. You will also need to implement *getKernel()* method.