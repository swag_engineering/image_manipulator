#include "CustomFrame3x3.hpp"
#include "Kernel.hpp"

#include <QGridLayout>
#include <QRegExpValidator>


CustomFrame3x3::CustomFrame3x3(QWidget* parent)
	: EmptyFrame(parent),
	size_{3}
{
	QGridLayout* grid = new QGridLayout(this);
	grid->setAlignment(Qt::AlignHCenter | Qt::AlignTop );
	grid->setSpacing(20);
	QSize lineEditSize = QSize(60, 30);

	for(unsigned int i=0; i<size_; ++i)
	{
		QList<QLineEdit*> row;
		for (unsigned int j=0; j<size_; ++j)
		{
			row.append(new QLineEdit("-1", this));
			row[j]->setFixedSize(lineEditSize);
			row[j]->setAlignment(Qt::AlignCenter);
			row[j]->setValidator(new QRegExpValidator(
				QRegExp("[+-]?([0-9]*[.])?[0-9]+"), this));
			grid->addWidget(row[j], i, j);
		}
		lineEditGrid_.append(row);
	}
}

Kernel CustomFrame3x3::getKernel()
{
	return Kernel(size_, &CustomFrame3x3::translator, this);
}

double CustomFrame3x3::getValue(int y, int x)
{
	return (double)lineEditGrid_[y][x]->text().toDouble();	
}

double CustomFrame3x3::translator(void* context, int y, int x)
{
	return static_cast<CustomFrame3x3*>(context)->getValue(y, x);
}