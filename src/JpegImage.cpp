#include "JpegImage.hpp"
#include "Kernel.hpp"


// from Qt 5.13 it will be possible to format image in place
// for now that's the only easy way I found
JpegImage::JpegImage(const QString& fileName, const QSize& size)
	: QImage(QImage(QImage(fileName).convertToFormat(
		QImage::Format_RGB888)).scaled(size, Qt::KeepAspectRatio)),
	channels_(3) {}


void JpegImage::convolute(const Kernel& kernel) {
	size_t kernelSize = kernel.size();
	size_t kernelRad = kernelSize/2;
	uint8_t* img = bits();
	uint8_t* new_img = new uint8_t[
		(bytesPerLine() + 2 * kernelRad * channels_) *
		(height() + 2 * kernelRad)
	];
	
	//add zero edges to img
	uint8_t* ptr = new_img;
	size_t edgesSize = 2 * kernelRad * channels_;
	size_t cropSize = (bytesPerLine() + 2 * kernelRad * channels_) * kernelRad;
	memset(ptr, 0, cropSize + edgesSize / 2);
	ptr += cropSize + edgesSize / 2;
	for(size_t i = 0; i < (size_t)height(); i++)
	{
		memcpy(ptr, &img[i * bytesPerLine()], bytesPerLine());
		ptr += bytesPerLine();
		memset(ptr, 0, edgesSize);
		ptr += edgesSize;
	}
	memset(ptr, 0, cropSize - edgesSize / 2);

	size_t rowSize = bytesPerLine() + edgesSize;
	size_t rowSizeLimit = bytesPerLine() - bytesPerLine() % channels_;
	for(size_t i_y = 0; i_y < (size_t)height(); i_y++) {
		for (size_t i_x = 0; i_x < rowSizeLimit; i_x++) {
			double result = 0;
			for(size_t k_y = 0; k_y < kernelSize; k_y++) {
				for (size_t k_x = 0; k_x < kernelSize; k_x++) {
					result += (double)new_img[
						((i_y + k_y) * (rowSize) + 
						(i_x + k_x * channels_))
					] * kernel[k_y][k_x];
				}
			}
			size_t pos = i_y * bytesPerLine() + i_x;
			if(result > 0xff) {
				img[pos] = 0xff;
			} else if(result < 0){
				img[pos] = 0;
			} else {
				img[pos] = (uint8_t)result;
			}
		}
	}
	delete [] new_img;
}