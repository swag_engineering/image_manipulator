#include "ImageWindow.hpp"
#include "JpegImage.hpp"
#include "Kernel.hpp"

#include <QGuiApplication>
#include <QScreen>
#include <QLayout>


ImageWindow::ImageWindow(QString& path, QWidget* parent)
	: QLabel(parent),
	imagePath_{path}
{
	setWindowTitle(" ");
	setWindowFlag(Qt::Dialog);
	windowSize_ = QGuiApplication::primaryScreen()->geometry().size() * 0.9;
	image_ = JpegImage(imagePath_);

	if(image_.width() > windowSize_.width() ||
		image_.height() > windowSize_.height())
	{
		image_ = JpegImage(imagePath_, windowSize_);
	} else {
		image_ = JpegImage(imagePath_, image_.size());
	}
	setPixmap(QPixmap::fromImage(image_));
	windowSize_ = image_.size();
	setFixedSize(windowSize_);
	show();
}

void ImageWindow::save(QString str)
{
	image_.save(str);
}

void ImageWindow::apply(const Kernel& kernel)
{
	image_.convolute(kernel);
	clear();
	setPixmap(QPixmap::fromImage(image_));
}

void ImageWindow::reload()
{	
	image_ = JpegImage(imagePath_, windowSize_);
	clear();
	setPixmap(QPixmap::fromImage(image_));
}

void ImageWindow::closeEvent(QCloseEvent* event)
{
	emit windowClosed();
	QWidget::closeEvent(event);
}