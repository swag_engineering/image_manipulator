#include "ImageManipulator.hpp"
#include "GaussianFrame.hpp"
#include "CustomFrame3x3.hpp"
#include "EmptyFrame.hpp"
#include "ImageWindow.hpp"

#include <QThread>


ImageManipulator::ImageManipulator(QWidget *parent)
	: QWidget(parent)
{
	setFixedSize(QSize(420, 480));
	setWindowTitle(" ");

	QHBoxLayout* mainLayout = new QHBoxLayout(this);

	QVBoxLayout* leftPanelLayout = new QVBoxLayout;
	mainLayout->addLayout(leftPanelLayout);
	QVBoxLayout* managmentLayout = new QVBoxLayout;
	mainLayout->addLayout(managmentLayout);

	const QSize buttonSize = QSize(75, 30);
	openButton_ = new QPushButton("Open");
	openButton_->setFixedSize(buttonSize);
	managmentLayout->addWidget(openButton_);
	connect(openButton_, SIGNAL(clicked()), this, SLOT(openImage()));

	reloadButton_ = new QPushButton("Reload");
	reloadButton_->setFixedSize(buttonSize);
	managmentLayout->addWidget(reloadButton_);
	connect(reloadButton_, &QPushButton::clicked, [=]() {
		dialog_->reload();
	});

	saveButton_ = new QPushButton("Save");
	saveButton_->setFixedSize(buttonSize);
	managmentLayout->addWidget(saveButton_);
	connect(saveButton_, SIGNAL(clicked()), this, SLOT(saveImage()));

	applyButton_ = new QPushButton("Apply");
	applyButton_->setFixedSize(buttonSize);
	managmentLayout->addWidget(applyButton_);
	connect(applyButton_, SIGNAL(clicked()), this, SLOT(applyFilter()));

	managmentLayout->addStretch();

	QHBoxLayout* selectorLayout = new QHBoxLayout;

	leftPanelLayout->addLayout(selectorLayout);
	selectorLayout->addStretch();
	filterSelector_ = new QComboBox();
	filterSelector_->setFixedSize(QSize(220, 30));
	selectorLayout->addWidget(filterSelector_);
	selectorLayout->addStretch();

	frameLayout_ = new QHBoxLayout;
	leftPanelLayout->addLayout(frameLayout_);

	filterSelectrionMap_["--"] = &createFrameInstance<EmptyFrame>;
	filterSelectrionMap_["Gaussian filter"] = &createFrameInstance<GaussianFrame>;
	filterSelectrionMap_["Custom 3x3 filter"] = &createFrameInstance<CustomFrame3x3>;

	for (QString& element : filterSelectrionMap_.keys())
	{
    	filterSelector_->addItem(element);
  	}

  	filterSelector_->setCurrentIndex(filterSelector_-> findText("--"));
  	frameLayout_->addStretch();
  	frame_ = filterSelectrionMap_["--"]();
  	frameLayout_->addWidget(frame_);
  	frameLayout_->addStretch();
    connect(filterSelector_, SIGNAL(currentIndexChanged(QString)), this, SLOT(filterChanged(QString)));

	setPanelEnabled(false);
}

void ImageManipulator::filterChanged(QString filterName)
{
	frameLayout_->removeWidget(frame_);
	delete frame_;
	frame_ = filterSelectrionMap_[filterName.toUtf8().constData()]();
	frameLayout_->insertWidget(frameLayout_->count()-1, frame_);
}

void ImageManipulator::openImage()
{
	QString imagePath = QFileDialog::getOpenFileName(this, "Open image", "./",
		"JPG (*.jpg *.jpeg, *.png, *.bmp)");
	if(imagePath == "")
	{
		return;
	}
	dialog_ = new ImageWindow(imagePath, this); // adding this throws segfault
	connect(dialog_, &ImageWindow::windowClosed, [=]() {
		setPanelEnabled(false);
		openButton_->setEnabled(true);
	});
	dialog_->setAttribute(Qt::WA_DeleteOnClose);
	setPanelEnabled(true);
	openButton_->setEnabled(false);
}

void ImageManipulator::setPanelEnabled(bool state)
{
	reloadButton_->setEnabled(state);
	saveButton_->setEnabled(state);
	applyButton_->setEnabled(state);
}

void ImageManipulator::saveImage()
{
	QString imagePath = QFileDialog::getSaveFileName(this, "Save image",
		"./untitled.jpg", "JPG (*.jpg *.jpeg)");
	if(imagePath == "")
		return;

	dialog_->save(imagePath);
}

void ImageManipulator::applyFilter()
{
	try {
		Kernel kernel = frame_->getKernel();
		QThread* thread = QThread::create([=]() {
			setPanelEnabled(false);
			dialog_->apply(kernel);
		});
		connect(thread, &QThread::finished, [this, thread]() {
			setPanelEnabled(true);
			thread->deleteLater();
		});
		thread->start();
	} catch(std::runtime_error& ex)
	{
		errorBox(ex.what());
		return;
	}
}

void ImageManipulator::errorBox(QString text)
{
	QMessageBox messageBox;
	messageBox.critical(0,"Error", text);
	messageBox.setFixedSize(500,200);	
}

void ImageManipulator::warningBox(QString text)
{
	QMessageBox messageBox;
	messageBox.warning(0,"Warning", text);
	messageBox.setFixedSize(500,200);	
}