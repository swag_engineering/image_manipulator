#include "Kernel.hpp"
#include <stdexcept>


Kernel::Kernel(unsigned int size, double (*valueSetter)(void*, int, int),
		void* context, bool normalize)
	:size_{size}
{
	double sum = 0;
	for(unsigned int i = 0; i < size_; i++) {
		kernel_.push_back(std::vector<double>(size_));
		for (unsigned int j = 0; j < size_; j++) {
			kernel_[i][j] = valueSetter(context, i, j);
			sum += kernel_[i][j];
		}
	}
	if(normalize) {
		if(sum == 0) {			
			throw std::runtime_error("Can't normalize, since summ is zero!");
		}
		for(unsigned int i = 0; i < size_; i++) {
			for (unsigned int j = 0; j < size_; j++) {
				kernel_[i][j] = kernel_[i][j]/sum;
			}
		}
	}

}