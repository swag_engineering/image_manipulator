#include "ImageManipulator.hpp"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ImageManipulator w;
    w.show();
    return a.exec();
}
