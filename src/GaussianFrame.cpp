#include "GaussianFrame.hpp"
#include "Kernel.hpp"

#include <QVBoxLayout>

# define M_PI		3.14159265358979323846

GaussianFrame::GaussianFrame(QWidget* parent)
	: EmptyFrame(parent),
	vectorSize_{500}
{
	QVBoxLayout* mainLayout = new QVBoxLayout(this);

	const double xLimitLow = -1;
	const double xLimitHeigh = 1;
	const double yLimitLow = 0;
	const double yLimitHeigh = 1;

	for (unsigned int i=0; i<vectorSize_; i++)
	{
		x_[i] = xLimitLow + i*(abs(xLimitLow) + abs(xLimitHeigh))/vectorSize_;
	}

	customPlot_ = new QCustomPlot;
	mainLayout->addWidget(customPlot_);
	customPlot_->setFixedSize(QSize(this->width(), this->width()*0.8));
	customPlot_->setBackground(this->palette().background().color());
	customPlot_->xAxis->setRange(xLimitLow, xLimitHeigh);
	customPlot_->yAxis->setRange(yLimitLow, yLimitHeigh);	
	customPlot_->addGraph();

	QLabel* formulaLabel = new QLabel;
	mainLayout->addWidget(formulaLabel);
	formulaLabel->setAlignment(Qt::AlignCenter);
	QPixmap formulaImage;
	// make an absolute path workaround
	formulaImage.load("./img/gaussian_formula.png");
	formulaLabel->setPixmap(formulaImage);

	QHBoxLayout* sigmaLayout = new QHBoxLayout;
	mainLayout->addLayout(sigmaLayout);
	QLabel* sigmaLabel = new QLabel("σ: ");
	sigmaLayout->addWidget(sigmaLabel);
	QSlider* sigmaSlider = new QSlider;
	sigmaLayout->addWidget(sigmaSlider);
	sigmaSlider->setOrientation(Qt::Horizontal);
	sigmaSlider->setFixedSize(150, 20);
	connect(sigmaSlider, SIGNAL(valueChanged(int)), this, SLOT(sigmaSliderChanged(int)));
	sigmaSlider->setSliderPosition(20);
	sigmaLayout->addStretch(1);

	QHBoxLayout* muLayout = new QHBoxLayout;
	mainLayout->addLayout(muLayout);
	QLabel* muLabel = new QLabel("μ: ");
	muLayout->addWidget(muLabel);

	QSlider* muSlider = new QSlider;
	muLayout->addWidget(muSlider);
	muSlider->setOrientation(Qt::Horizontal);
	muSlider->setFixedSize(150, 20);
	connect(muSlider, SIGNAL(valueChanged(int)), this, SLOT(muSliderChanged(int)));
	muSlider->setSliderPosition(50);
	muLayout->addStretch(1);

	QHBoxLayout* kernelLayout = new QHBoxLayout;
	mainLayout->addLayout(kernelLayout);
	QLabel* kernelLabel = new QLabel("Kernel size: ");
	kernelLayout->addWidget(kernelLabel);

	kernelSpinBox_ = new QSpinBox();
	kernelLayout->addWidget(kernelSpinBox_);
	kernelLayout->addStretch(1);
	//Make workaround to make text in lineedit notselectable on focus
	kernelSpinBox_->findChild<QLineEdit*>()->setReadOnly(true);
	kernelSpinBox_->setRange(3, 15);
	kernelSpinBox_->setSingleStep(2);
	kernelSpinBox_->setFixedSize(40, 20);
}

void GaussianFrame::muSliderChanged(int value)
{
	muValue_ = (double)value/10 - 5;
	redrawPlot();
}

void GaussianFrame::sigmaSliderChanged(int value)
{
	sigmaValue_ = pow(1.2, (double)value/20) - 0.6;
	redrawPlot();
}

void GaussianFrame::redrawPlot()
{
	for (unsigned int i=0; i<vectorSize_; i++)
	{
		y_[i] = (1/(sigmaValue_ * sqrt(2 * M_PI))) * exp(
			-pow(x_[i] - muValue_, 2) / (2 * pow(sigmaValue_, 2)));
	}
	customPlot_->graph(0)->setData(x_, y_);
	customPlot_->replot();
}

Kernel GaussianFrame::getKernel()
{
	return Kernel((unsigned int)kernelSpinBox_->value(), &GaussianFrame::translator, this, true);
}

double GaussianFrame::getValue(int y, int x)
{
	unsigned int size = (unsigned int)kernelSpinBox_->value();
	double middleX = (double)(x) - (double)(size-1)/2;
	double middleY = (double)(y) - (double)(size-1)/2;
	double distance = sqrt(pow(middleX, 2) + pow(middleY, 2)) * (2*(size-1)-1)/(2*(size-1));
	double distanceSigned = (middleX < 0) ? -distance : distance;
	double distanceNormalized = distanceSigned/sqrt(2*pow(size-1, 2));
	unsigned int vectorIdx = (unsigned int)(vectorSize_ - 1)/2 + (vectorSize_ - 1) * distanceNormalized;
	return y_[vectorIdx];
}

double GaussianFrame::translator(void* context, int y, int x)
{
	return static_cast<GaussianFrame*>(context)->getValue(y, x);
}